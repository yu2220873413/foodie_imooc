package com.imooc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @description:
 * @author: 余希瑶
 * @date: 2021年05月09日 15:44
 * @version:1.0
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return new Date().toString();
    }

}
