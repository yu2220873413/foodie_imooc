package com.imooc.controller;

import com.imooc.pojo.Stu;
import com.imooc.service.StuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @description:
 * @author: 余希瑶
 * @date: 2021年05月09日 15:44
 * @version:1.0
 */
@RestController
public class StuFooController {

    @Autowired
    private StuService stuService;


    @GetMapping("/getStu")
    public Object getStu(int id){
        Stu stuInfo = stuService.getStuInfo(id);

        return stuInfo;
    }

    @PostMapping("/saveStu")
    public Object saveStu(int id){
       stuService.saveStu();
        return "OK";
    }

    @PutMapping("/updateStu")
    public Object updateStu(int id){
        stuService.updateStu(id);
        return "OK";
    }

    @DeleteMapping("/deleteStu")
    public Object deleteStu(int id){
        stuService.deleteStu(id);
        return "OK";
    }

}
