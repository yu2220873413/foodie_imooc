package com.imooc.service;

import com.imooc.pojo.Stu;

/**
 * @description:
 * @author: 余希瑶
 * @date: 2021年05月09日 19:47
 * @version:1.0
 */
public interface StuService {

    public Stu getStuInfo(int id);

    public void saveStu();

    public void updateStu(int id);

    public void deleteStu(int id);


    public void saveParent();
    public void saveChildren();

}
